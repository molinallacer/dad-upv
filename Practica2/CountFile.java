package hola;
import java.io.*;

public class CountFile {
  public static void main (String[] args)
      throws java.io.IOException, java.io.FileNotFoundException {
    int count= 0;
    InputStream is= null;
    String filename = null;
    if (args.length >= 1) {
/* COMPLETAR: is= Cree una instància de FileInputStream
 * per a llegir del fitxer passat com argument a args[0]
 */
      filename= args[0];
      /* 1 */
      is = new FileInputStream(filename);
      /* 1 */
    } else {
      is= System.in;
      filename= "Input";
    }
/* COMPLETAR: Utilitze amb while (...) count++;
 * un mètode de FileInputSream per a llegir un caracter
 */
/* 2 */
    while (is.read() != -1) count++;
/* 2 */
    is.close();
    System.out.println(filename + " has " + count + " chars.");
  }
}
