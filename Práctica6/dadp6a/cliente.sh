#!/bin/bash

if [ $# -lt 1 ]
then
    echo "N�mero de par�metros incorrecto."
    exit
fi

a=`pwd`

java -cp . -Djava.rmi.server.codebase=file://$a -Djava.security.policy=client.policy echoRmi.client.EchoRMI $1