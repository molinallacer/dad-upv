#!/bin/bash

if [ $# -lt 1 ]
then
    echo "N�mero de par�metros incorrecto."
    exit
fi

a=`pwd`

java -cp . -Djava.rmi.server.codebase=file://$a -Djava.rmi.server.hostname=$1 -Djava.security.policy=server.policy echoRmi.server.EchoObjectRMI