package client;

import java.io.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import compute.Compute;
import engine.EchoObject;

public class ComputePi {
  public static void main(String[] args) {
    if (System.getSecurityManager() == null) {
      System.setSecurityManager(new SecurityManager());
    }

    try {
      String name= "Compute";
      Registry registry= LocateRegistry.getRegistry(args[0]);
      Compute comp= (Compute) registry.lookup(name);

      EchoTask task= new EchoTask();
      comp.loadTask(task);

      Object obj= comp.executeTask(args[1]);
	  System.out.println("Servidor: " + obj.toString());
    }
    catch (Exception e) {
      System.err.println("ComputePi exception:");
      e.printStackTrace();
    }
  }
}